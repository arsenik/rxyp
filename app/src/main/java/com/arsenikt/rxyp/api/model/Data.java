package com.arsenikt.rxyp.api.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "data")
public class Data {
    @Element(name = "images")
    public Images images;

    public Data() {
    }
}