package com.arsenikt.rxyp.api.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by arsenik on 3/30/16.
 */
@Root(name = "images")
public class Images {
    @ElementList(entry = "image", inline = true)
    public List<Image> images;

    public Images() {}
}
