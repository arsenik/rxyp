package com.arsenikt.rxyp.api.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by arsenik on 3/30/16.
 */
@Root(name = "image", strict = false)
public class Image {
    @Element(name = "id")
    public String id;

    @Element(name = "url")
    public String url;

    @Element(name = "source_url")
    public String sourceUrl;

    public Image() {

    }
}
