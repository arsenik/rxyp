package com.arsenikt.rxyp.api;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by arsenik on 3/30/16.
 */
public class ApiRetrofit {
    public static Retrofit get() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiSettings.API_BASE_URL)
                .client(new OkHttpClient())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        return retrofit;
    }
}
