package com.arsenikt.rxyp.api.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by arsenik on 3/30/16.
 */
@Root(name = "response")
public class Response {

    @Element(name = "data")
    public Data data;

    public Response() {}
}
