package com.arsenikt.rxyp.api;

import com.arsenikt.rxyp.api.model.Response;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by arsenik on 3/30/16.
 */
public interface CatService {
    @GET("images/get")
    Observable<Response> data(@Query("format") String format, @Query("type") String type);
}
