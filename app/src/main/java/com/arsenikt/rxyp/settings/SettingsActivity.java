package com.arsenikt.rxyp.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.arsenikt.rxyp.R;

import java.util.Map;

/**
 * Created by arsenik on 11/12/15.
 */
public class SettingsActivity extends PreferenceActivity {
    private SharedPreferences.OnSharedPreferenceChangeListener mPrefChangeListener = new SharedPreferences
            .OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPrefs,
                                              String key) {
            SettingsFragment settingFragment = (SettingsFragment) getFragmentManager().findFragmentById(android.R
                    .id.content);
            settingFragment.getChildFragmentManager().executePendingTransactions();

            String updateInterval = sharedPrefs.getString("updateInterval", String.valueOf(0));
            Log.d("updateInterval", "updateInterval: " + updateInterval);

            settingFragment.updateIntervalUI(sharedPrefs);
            settingFragment.updateCurrentTagUI(sharedPrefs);
            settingFragment.updateCensoredUI(sharedPrefs);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPref.registerOnSharedPreferenceChangeListener(mPrefChangeListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(mPrefChangeListener);
    }

    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.user_settings);
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

            updateIntervalUI(sharedPref);
            updateCurrentTagUI(sharedPref);
            updateCensoredUI(sharedPref);
        }

        public void updateCensoredUI(SharedPreferences sharedPref) {
            CheckBoxPreference censoredCheckBox = (CheckBoxPreference) findPreference("censoredAdvices");

            if (sharedPref.getBoolean("showByTagEnabled", false)) {
                censoredCheckBox.setChecked(false);
                censoredCheckBox.setEnabled(false);
            } else {
                censoredCheckBox.setEnabled(true);
                censoredCheckBox.setChecked(sharedPref.getBoolean("censoredAdvices", false));
            }
        }

        public void updateCurrentTagUI(SharedPreferences sharedPref) {
            String[] tagsName = getResources().getStringArray(R.array.tags);
            String tagValue = sharedPref.getString("currentShowTag", tagsName[0]);

            ListPreference tagPref = (ListPreference) findPreference("currentShowTag");
            tagPref.setSummary(tagValue);
        }

        public void updateIntervalUI(SharedPreferences sharedPref) {
            String intervalValue = sharedPref.getString("updateInterval", String.valueOf(0));
            Map<String, String> intervals = SettingsUtil.getIntervalsMap(getActivity());

            ListPreference updateIntervalPref = (ListPreference) findPreference("updateInterval");
            updateIntervalPref.setSummary(intervals.get(intervalValue));
        }
    }

}
