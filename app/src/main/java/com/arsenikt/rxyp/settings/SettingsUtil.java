package com.arsenikt.rxyp.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.arsenikt.rxyp.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by arsenik on 11/16/15.
 */
public class SettingsUtil {

    public static final String IMAGE_URL = "imageUrl";

    public static Map<String, Integer> getTagsMap(Context context) {
        HashMap<String, Integer> tagsMap = new HashMap<String, Integer>();
        String[] tagsName = context.getResources().getStringArray(R.array.tags);
        String[] tagsVals = context.getResources().getStringArray(R.array.tags_val);
        for (int i = 0; i < tagsName.length; i++) {
            tagsMap.put(tagsName[i], Integer.valueOf(tagsVals[i]));
        }
        return tagsMap;
    }

    public static Map<String, String> getIntervalsMap(Context context) {
        HashMap<String, String> intervalMap = new HashMap<String, String>();
        String[] intervalValue = context.getResources().getStringArray(R.array.interval_vals);
        String[] intervalTitle = context.getResources().getStringArray(R.array.interval);
        for (int i = 0; i < intervalValue.length; i++) {
            intervalMap.put(intervalValue[i], intervalTitle[i]);
        }
        return intervalMap;
    }

    private static SharedPreferences getPrefs(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("rxypPrefs", Context.MODE_PRIVATE);
        return sharedPref;
    }

    public static String getImageUrl(Context context) {
        String url = getPrefs(context).getString(IMAGE_URL, "");
        Log.d("saveImageUrl", "get " + url);
        return url;
    }

    public static void saveImageUrl(Context context, String imageUrl) {
        Log.d("saveImageUrl", "saveImageUrl " + imageUrl);
        getPrefs(context).edit().putString(IMAGE_URL, imageUrl).apply();
    }
}
