package com.arsenikt.rxyp.bs.widget;

import android.content.Context;
import android.util.Log;
import android.widget.RemoteViews;

import com.arsenikt.rxyp.R;
import com.arsenikt.rxyp.api.ApiRetrofit;
import com.arsenikt.rxyp.api.CatService;
import com.arsenikt.rxyp.api.model.Response;
import com.arsenikt.rxyp.settings.SettingsUtil;

import retrofit2.Retrofit;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by arsenik on 3/30/16.
 */
public class CatWidget extends BsWidget {
    @Override
    public RemoteViews draw(Context context, boolean isWhiteDevice) {
        RemoteViews remoteViews = null;
        if (isWhiteDevice) {
            remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_cat_b);
        } else {
            remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_cat_b);
        }
        remoteViews.setOnClickPendingIntent(R.id.image, getUpdateIntent(context));
        return remoteViews;
    }



    private void loadImage(Subscriber<Response> subscriber) {
        Retrofit retrofit = ApiRetrofit.get();
        CatService catService = retrofit.create(CatService.class);
        catService.data("xml", "png,jpg")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    public void update(final Context context) {
        Log.d("saveImageUrl", "update");
        loadImage(new Subscriber<Response>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.d("imageUrlLoad", "[Error] " + e.getMessage());
            }

            @Override
            public void onNext(Response response) {
                String imageUrl = response.data.images.images.get(0).url;
                Log.d("imageUrlLoad", "[onNext] " + imageUrl);
                SettingsUtil.saveImageUrl(context, imageUrl);
                redrawWidgets(context);
            }
        });
    }
}
