package com.arsenikt.rxyp.bs.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.arsenikt.rxyp.R;
import com.arsenikt.rxyp.settings.SettingsUtil;
import com.squareup.picasso.Picasso;
import com.yotadevices.sdk.BackscreenLauncherConstants;

/**
 * Created by arsenik on 3/30/16.
 */
public abstract class BsWidget extends AppWidgetProvider {
    public static final String ACTION_UPDATE_WIDGET_DATA = "ACTION_UPDATE_WIDGET_DATA";

    public abstract RemoteViews draw(Context context, boolean isWhiteDevice);
    public abstract void update(Context context);

    @Override
    public void onAppWidgetOptionsChanged(final Context context, AppWidgetManager appWidgetManager, final int wId,
                                          Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, wId, newOptions);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        redrawWidgets(context);
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction().equals(ACTION_UPDATE_WIDGET_DATA)) {
            update(context);
        }

    }

    public void redrawWidgets(final Context context) {
        ComponentName componentName = new ComponentName(context, CatWidget.class);
        int[] allWidgetIds = AppWidgetManager.getInstance(context).getAppWidgetIds(componentName);

        for (int j = 0; j < allWidgetIds.length; j++) {
            final int wId = allWidgetIds[j];
            performWidgetDraw(context, wId, SettingsUtil.getImageUrl(context));
        }
    }

    private void performWidgetDraw(final Context context, final int wId, final String imageUrl) {
        if (TextUtils.isEmpty(imageUrl))
            return;
        Bundle appWidgetOptions = AppWidgetManager.getInstance(context).getAppWidgetOptions(wId);
        final RemoteViews remoteViews = draw(context, isWhiteDevice(appWidgetOptions));
        Picasso.with(context).load(imageUrl).into(remoteViews, R.id.image, new int[] {wId});
        AppWidgetManager.getInstance(context).updateAppWidget(wId, remoteViews);
    }

    private static boolean isWhiteDevice(Bundle appWidgetOptions) {
        int theme = appWidgetOptions.getInt(BackscreenLauncherConstants.OPTION_WIDGET_THEME, -1);
        return theme == BackscreenLauncherConstants.WIDGET_THEME_WHITE;
    }

    protected PendingIntent getUpdateIntent(Context context) {
        Intent intent = new Intent(context, CatWidget.class);
        intent.setAction(ACTION_UPDATE_WIDGET_DATA);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return pendingIntent;
    }
}
